## Exercice 1 - Questions sur les acquis, notions vues en cours

### Question 1 : Quelles sont les principales sources de complexité dans un système logiciel (sources d’un programme) ?
La complexité dans les systèmes logiciels peut provenir de plusieurs sources :

- **Complexité inhérente :** Elle découle du domaine du problème lui-même, souvent inévitable.
- **Complexité accidentelle :** Introduite par les choix technologiques, les outils, et les méthodes de développement utilisés. Elle peut être réduite ou éliminée par des choix de conception et d'outils plus appropriés.
- **Complexité due à l'évolution :** Survient lorsque les exigences du système changent ou évoluent au fil du temps, entraînant une augmentation de la complexité pour intégrer de nouvelles fonctionnalités ou maintenir des fonctionnalités existantes.
- **Complexité opérationnelle :** Liée à la configuration, au déploiement, et à la maintenance de l'environnement de production du système.

### Question 2 : Quel(s) avantage(s) procure le fait de programmer vers une interface et non vers une implémentation ?
Programmer vers une interface plutôt que vers une implémentation permet de définir des contrats clairs entre différentes parties d'un système, ce qui facilite la modularité et la substitution. Cela rend le code moins couplé et plus flexible, permettant de changer les implémentations sans modifier le code qui utilise ces interfaces. Cela facilite également les tests, car les implémentations peuvent être facilement remplacées par des mock-ups lors des tests.

### Question 3 : Comment comprenez-vous chaque étape de cette heuristique ?
- **Faites en sorte que ça fonctionne :** Commencez par développer une version de base du système qui réalise les fonctions essentielles, même si elle n'est pas parfaite ou complète.
- **Assurez-vous que ce soit correct :** Une fois que le système fonctionne de manière basique, concentrez-vous sur la correction des bugs et assurez-vous que toutes les fonctionnalités répondent aux exigences et sont fiables.
- **Préoccupez-vous de le rendre rapide :** Après avoir stabilisé le système, optimisez les performances pour répondre aux critères de temps de réponse et d'efficacité requis.

### Question 4 : Quelle méthode ou approche est recommandée pour mener à bien un travail de refactoring ?
Le refactoring doit être effectué de manière systématique et contrôlée. Il est recommandé d'utiliser des tests unitaires pour s'assurer que le comportement du système reste inchangé après le refactoring. Il est crucial d'effectuer le refactoring par petites étapes, en vérifiant à chaque étape que le système fonctionne toujours correctement, et d'utiliser des outils de versioning comme Git pour gérer et documenter les modifications.

## Exercice 2 - Refactoring

### Pour Commencer
clonez le dépôt et configurez un environnement PHP pour exécuter le script.

### Installation
1. Clonez le dépôt :
   ```bash
   git clone https://votre-url-de-depot-gitlab.git

2. Naviguez vers le répertoire du projet :
    ```bash
   cd chemin-vers-le-projet

3. Exécuter le Script :
    ```bash
   php projet/invoice.php
