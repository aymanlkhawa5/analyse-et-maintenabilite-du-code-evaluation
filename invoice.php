<?php

class Car
{
    const REGULAR = 0;
    const NEW_MODEL = 1;

    private string $_title;
    private array $_priceCode; // Now an array to hold price settings

    public function __construct(string $title, array $priceCode)
    {
        $this->_title = $title;
        $this->_priceCode = $priceCode;
    }

    public function getPriceCode(): array
    {
        return $this->_priceCode;
    }

    public function setPriceCode(array $priceCode): void
    {
        $this->_priceCode = $priceCode;
    }

    public function getTitle(): string
    {
        return $this->_title;
    }
}

class Rental
{
    private Car $_car;
    private int $_daysRented;

    public function __construct(Car $car, int $daysRented)
    {
        $this->_car = $car;
        $this->_daysRented = $daysRented;
    }

    public function getDaysRented(): int
    {
        return $this->_daysRented;
    }

    public function getCar(): Car
    {
        return $this->_car;
    }
}

class Customer
{
    private string $_name;
    private array $_rentals = [];

    public function __construct(string $name)
    {
        $this->_name = $name;
    }

    public function addRental(Rental $arg): void
    {
        $this->_rentals[] = $arg;
    }

    public function getName(): string
    {
        return $this->_name;
    }

    public function invoice($format = 'TEXT'): string
    {
        $totalAmount = 0.0;
        $frequentRenterPoints = 0;
        $result = "Rental Record for " . $this->getName() . "\n";
        $invoiceDetails = [];

        foreach ($this->_rentals as $each) {
            $thisAmount = $each->getCar()->getPriceCode()['baseRate'] + 
                          $each->getDaysRented() * $each->getCar()->getPriceCode()['dailyRate'];

            // Adjust discounts
            if ($each->getDaysRented() > $each->getCar()->getPriceCode()['discountDays']) {
                $thisAmount -= ($each->getDaysRented() - $each->getCar()->getPriceCode()['discountDays']) * 
                               $each->getCar()->getPriceCode()['discountRate'];
            }

            $frequentRenterPoints += floor($thisAmount * 0.10);  // 10% of the total amount as points

            $invoiceDetails[] = [
                'title' => $each->getCar()->getTitle(),
                'thisAmount' => $thisAmount
            ];

            $totalAmount += $thisAmount;
        }

        if ($format === 'JSON') {
            return json_encode([
                'customerName' => $this->getName(),
                'totalAmount' => $totalAmount,
                'frequentRenterPoints' => $frequentRenterPoints,
                'rentals' => $invoiceDetails
            ]);
        } else {
            foreach ($invoiceDetails as $detail) {
                $result .= "\t" . $detail['title'] . "\t" . number_format($detail['thisAmount'], 2) . "\n";
            }
            $result .= "Amount owed is " . number_format($totalAmount, 2) . "\n";
            $result .= "You earned " . $frequentRenterPoints . " frequent renter points\n";
            return $result;
        }
    }
}

// Example with dynamic pricing rules:
$car1 = new Car("Car 1", ['baseRate' => 5000, 'dailyRate' => 9500, 'discountDays' => 5, 'discountRate' => 10000]);
$car2 = new Car("Car 2", ['baseRate' => 9000, 'dailyRate' => 15000, 'discountDays' => 3, 'discountRate' => 10000]);

$rental1 = new Rental($car1, 3);
$rental2 = new Rental($car2, 2);

$customer = new Customer("John Doe");
$customer->addRental($rental1);
$customer->addRental($rental2);

echo $customer->invoice(); // Default TEXT output
echo $customer->invoice('JSON'); // JSON output

